#!/bin/bash
###################################################################################
#@ Script: run-sshd.sh
#@ Description: Préaction et Lancement du sshd
#@ Utilisation:
#@   ./run-sshd.sh
###################################################################################

echo "run-sshd.sh : Lancement de sshd"
set -e

###################################################################################
# Ajout de la clef ssh de ansible
echo "ANSIBLE_SSH_KEY=$ANSIBLE_SSH_KEY"

cat > /home/ansible/.ssh/authorized_keys << EOF
$ANSIBLE_SSH_KEY
EOF

chown ansible:ansible /home/ansible/.ssh/authorized_keys
###################################################################################

###################################################################################
# Lancement de sshd
exec /usr/sbin/sshd -D
###################################################################################
